;              
CREATE USER IF NOT EXISTS "SA" SALT 'e2df9add5abde992' HASH '9ca4e50f3ae390188287ae9fe50d82b683db90f3aecb6eae35402a13ca223e51' ADMIN;          
CREATE SEQUENCE "PUBLIC"."HIBERNATE_SEQUENCE" START WITH 1;    
CREATE SEQUENCE "PUBLIC"."SYSTEM_SEQUENCE_44ADEAEB_1D85_4CF2_8A94_10BF0BDF4498" START WITH 80 BELONGS_TO_TABLE;
CREATE MEMORY TABLE "PUBLIC"."SECTOR"(
    "ID" BIGINT DEFAULT NEXT VALUE FOR "PUBLIC"."SYSTEM_SEQUENCE_44ADEAEB_1D85_4CF2_8A94_10BF0BDF4498" NOT NULL NULL_TO_DEFAULT SEQUENCE "PUBLIC"."SYSTEM_SEQUENCE_44ADEAEB_1D85_4CF2_8A94_10BF0BDF4498",
    "NAME" VARCHAR(255),
    "VALUE" VARCHAR(255)
);          
ALTER TABLE "PUBLIC"."SECTOR" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_9" PRIMARY KEY("ID");        
-- 79 +/- SELECT COUNT(*) FROM PUBLIC.SECTOR;  
INSERT INTO "PUBLIC"."SECTOR" VALUES
(1, 'Manufacturing', '1'),
(2, '&nbsp;&nbsp;&nbsp;&nbsp;Construction materials', '19'),
(3, '&nbsp;&nbsp;&nbsp;&nbsp;Electronics and Optics', '18'),
(4, '&nbsp;&nbsp;&nbsp;&nbsp;Food and Beverage', '6'),
(5, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bakery &amp; confectionery products', '342'),
(6, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Beverages', '43'),
(7, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fish &amp; fish products ', '42'),
(8, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Meat &amp; meat products', '40'),
(9, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Milk &amp; dairy products ', '39'),
(10, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other', '437'),
(11, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sweets &amp; snack food', '378'),
(12, '&nbsp;&nbsp;&nbsp;&nbsp;Furniture', '13'),
(13, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bathroom/sauna ', '389'),
(14, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bedroom', '385'),
(15, STRINGDECODE('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Children\u2019s room '), '390'),
(16, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kitchen ', '98'),
(17, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Living room ', '101'),
(18, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Office', '392'),
(19, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other (Furniture)', '394'),
(20, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Outdoor ', '341'),
(21, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Project furniture', '99'),
(22, '&nbsp;&nbsp;&nbsp;&nbsp;Machinery', '12'),
(23, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Machinery components', '94'),
(24, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Machinery equipment/tools', '91'),
(25, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Manufacture of machinery ', '224'),
(26, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maritime', '97'),
(27, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Aluminium and steel workboats ', '271'),
(28, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Boat/Yacht building', '269'),
(29, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ship repair and conversion', '230'),
(30, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Metal structures', '93'),
(31, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other', '508'),
(32, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Repair and maintenance service', '227'),
(33, '&nbsp;&nbsp;&nbsp;&nbsp;Metalworking', '11'),
(34, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Construction of metal structures', '67'),
(35, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Houses and buildings', '263'),
(36, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Metal products', '267'),
(37, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Metal works', '542'),
(38, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CNC-machining', '75'),
(39, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Forgings, Fasteners ', '62'),
(40, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gas, Plasma, Laser cutting', '69'),
(41, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MIG, TIG, Aluminum welding', '66'),
(42, '&nbsp;&nbsp;&nbsp;&nbsp;Plastic and Rubber', '9'),
(43, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Packaging', '54'),
(44, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plastic goods', '556'),
(45, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plastic processing technology', '559'),
(46, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Blowing', '55'),
(47, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Moulding', '57'),
(48, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plastics welding and processing', '53'),
(49, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plastic profiles', '560'),
(50, '&nbsp;&nbsp;&nbsp;&nbsp;Printing ', '5');
INSERT INTO "PUBLIC"."SECTOR" VALUES
(51, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Advertising', '148'),
(52, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Book/Periodicals printing', '150'),
(53, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Labelling and packaging printing', '145'),
(54, '&nbsp;&nbsp;&nbsp;&nbsp;Textile and Clothing', '7'),
(55, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clothing', '44'),
(56, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Textile', '45'),
(57, '&nbsp;&nbsp;&nbsp;&nbsp;Wood', '8'),
(58, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other (Wood)', '337'),
(59, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wooden building materials', '51'),
(60, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wooden houses', '47'),
(61, 'Other', '3'),
(62, '&nbsp;&nbsp;&nbsp;&nbsp;Creative industries', '37'),
(63, '&nbsp;&nbsp;&nbsp;&nbsp;Energy technology', '29'),
(64, '&nbsp;&nbsp;&nbsp;&nbsp;Environment', '33'),
(65, 'Service', '2'),
(66, '&nbsp;&nbsp;&nbsp;&nbsp;Business services', '25'),
(67, '&nbsp;&nbsp;&nbsp;&nbsp;Engineering', '35'),
(68, '&nbsp;&nbsp;&nbsp;&nbsp;Information Technology and Telecommunications', '28'),
(69, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data processing, Web portals, E-marketing', '581'),
(70, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Programming, Consultancy', '576'),
(71, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Software, Hardware', '121'),
(72, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Telecommunications', '122'),
(73, '&nbsp;&nbsp;&nbsp;&nbsp;Tourism', '22'),
(74, '&nbsp;&nbsp;&nbsp;&nbsp;Translation services', '141'),
(75, '&nbsp;&nbsp;&nbsp;&nbsp;Transport and Logistics', '21'),
(76, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Air', '111'),
(77, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rail', '114'),
(78, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Road', '112'),
(79, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Water', '113');  
CREATE MEMORY TABLE "PUBLIC"."USER"(
    "ID" BIGINT NOT NULL,
    "NAME" VARCHAR(255),
    "TERMS" BOOLEAN
); 
ALTER TABLE "PUBLIC"."USER" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_2" PRIMARY KEY("ID");          
-- 0 +/- SELECT COUNT(*) FROM PUBLIC.USER;     
CREATE MEMORY TABLE "PUBLIC"."USER_SECTORS"(
    "USER_ID" BIGINT NOT NULL,
    "SECTORS_ID" BIGINT NOT NULL
);
ALTER TABLE "PUBLIC"."USER_SECTORS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_5" PRIMARY KEY("USER_ID", "SECTORS_ID");               
-- 0 +/- SELECT COUNT(*) FROM PUBLIC.USER_SECTORS;             
ALTER TABLE "PUBLIC"."USER_SECTORS" ADD CONSTRAINT "PUBLIC"."FKN6OM5RRBGD4A6ELF1MQ70G0KB" FOREIGN KEY("USER_ID") REFERENCES "PUBLIC"."USER"("ID") NOCHECK;     
ALTER TABLE "PUBLIC"."USER_SECTORS" ADD CONSTRAINT "PUBLIC"."FK4ODBLUHYY3LP2RALWR72G1NGR" FOREIGN KEY("SECTORS_ID") REFERENCES "PUBLIC"."SECTOR"("ID") NOCHECK;
