In order to run the app:

gradle bootRun

Gradle wrapper is also present.

The default port is 8080. If it's already taken run:

SERVER_PORT={port_number} gradle bootRun

The app will be available at:

http://localhost:{port_number}/index

H2 in-memory database is used, so the schema is generated in real-time from entity classes.

H2 console is available at:

http://localhost:{port_number}/h2-console

DB name is jdbc:h2:mem:demo

User = sa
Password is empty.

Sectors are inserted upon app startup from src/main/resources/data.sql
