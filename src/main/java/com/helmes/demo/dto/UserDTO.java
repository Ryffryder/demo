package com.helmes.demo.dto;

import java.util.Set;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.helmes.demo.entities.Sector;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDTO {

    @NotBlank(message = "{user.name.blank}")
    @Size(min = 6, max = 60, message = "{user.name.size}")
    @Pattern(regexp = "^[\\p{L} .'-]+$", message = "{user.name.format}")
    private String name;

    @NotEmpty(message = "{user.sectors.empty}")
    @Size(min = 1, max = 5, message = "{user.sectors.size}")
    private Set<Sector> sectors;

    @NotNull(message = "{user.terms.null}")
    private Boolean terms;
}
