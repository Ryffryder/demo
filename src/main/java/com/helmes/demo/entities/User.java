package com.helmes.demo.entities;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.helmes.demo.dto.UserDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @ManyToMany
    private Set<Sector> sectors;

    private Boolean terms;

    public User(UserDTO dto) {
        this.name = dto.getName();
        this.sectors = dto.getSectors();
        this.terms = dto.getTerms();
    }
}
