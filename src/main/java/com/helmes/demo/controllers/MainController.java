package com.helmes.demo.controllers;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.helmes.demo.Constants;
import com.helmes.demo.dao.SectorDAO;
import com.helmes.demo.dao.UserDAO;
import com.helmes.demo.dto.UserDTO;
import com.helmes.demo.entities.Sector;
import com.helmes.demo.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MainController {

    private final UserDAO userDAO;
    private final List<Sector> sectors;

    @Autowired
    public MainController(SectorDAO sectorDAO, UserDAO userDAO) {
        this.userDAO = userDAO;
        this.sectors = sectorDAO.findAll();
    }

    @GetMapping(Constants.Paths.INDEX)
    public String index(Model model) {

        model.addAllAttributes(Map.of(
                "sectors", this.sectors,
                "userDTO", new UserDTO()));

        return "index";
    }

    @PostMapping(Constants.Paths.USER)
    public String user(@Valid UserDTO userDTO, BindingResult result,
                       Model model, HttpSession session) {

        model.addAttribute("sectors", this.sectors);

        if (result.hasErrors()) {
            return "index";
        }

        User user;
        Long userId = (Long) session.getAttribute("userId");
        if (userId != null && userDAO.findById(userId).isPresent()) {
            user = userDAO.findById(userId).get();

            user.setName(userDTO.getName());
            user.setSectors(userDTO.getSectors());
            user.setTerms(userDTO.getTerms());

            userDAO.save(user);
        } else {
            user = userDAO.save(new User(userDTO));

            session.setAttribute("userId", user.getId());
        }

        model.addAttribute("user", user);

        return "index";
    }
}
