package com.helmes.demo;

public final class Constants {

    private Constants() {
    }

    public static final class Paths {
        public static final String INDEX = "/index";
        public static final String USER = "/user";
    }
}
