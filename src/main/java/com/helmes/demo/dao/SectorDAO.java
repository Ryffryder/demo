package com.helmes.demo.dao;

import com.helmes.demo.entities.Sector;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SectorDAO extends JpaRepository<Sector, Long> {
}
